#include "opencv2/opencv.hpp"
#include <string>
#include <vector>

using namespace std;

// zaa xd

class Camera {
    public:
        Camera(int camID, size_t width, size_t height): _camID(camID), _cap(_camID), _width(width), _height(height) {
            _cap.set(CV_CAP_PROP_FRAME_WIDTH, _width);
            _cap.set(CV_CAP_PROP_FRAME_HEIGHT, _height);
        };
        
        ~Camera() {
            _cap.release();
        }

        bool read(cv::Mat& frame) {
            return _cap.isOpened() and _cap.read(frame);
        }
    
    private:
        int _camID;
        cv::VideoCapture _cap;
        size_t _width;
        size_t _height;
};

std::vector<cv::Mat> splitFrameVertical(cv::Mat& frame) {
    auto height = frame.size().height;
    auto width  = frame.size().width;
    auto left_frame = frame(cv::Rect(0, 0, width/2, height));
    auto right_frame = frame(cv::Rect(width/2, 0, width/2, height));
    return {left_frame, right_frame};
}

int main() {
    Camera cam(0, 1280, 480);
    cv::Mat frame;
    

    while(true) {
        cam.read(frame);
        auto frames = splitFrameVertical(frame);
        cv::imshow( "left", frames[0] );
        cv::imshow( "right", frames[1] );
        if( cv::waitKey(33) >= 0 ) break;
    }
    
return 0;
}
